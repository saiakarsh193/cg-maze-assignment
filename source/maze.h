#ifndef MAZE_H
#define MAZE_H

#include "main.h"

class Maze
{
	public:
		int gridsize;
		Maze(int gridsize);
		float * getwallverts();
		int edgewalls[1000];
		int noe;
		float verts[1000];
		int nof; //no of floats
	private:
		std::pair <int, int> get_coor(int grid, int node);
		int get_node(int grid, int row, int column);
		std::pair <int, int> get_ends(int grid, int edge);
		int find_rep(int n, int rep[]);
		void add_edge(int x, int y, int rep[], int size[]);
};

#endif