#include "maze.h"

using namespace std;

Maze::Maze(int gridsize)
{
	this->gridsize = gridsize;
}

pair <int, int> Maze::get_coor(int grid, int node)
{
	pair <int, int> ans;
	ans.first = node / grid;
	ans.second = node - ans.first * grid;
	return ans;
}

int Maze::get_node(int grid, int row, int column)
{
	return grid * row + column;
}

pair <int, int> Maze::get_ends(int grid, int edge)
{
	pair<int, int> ans;
	if(edge < grid * (grid - 1))
	{
		int row = edge / (grid - 1);
		int column = edge - (row * (grid - 1));
		ans.first = get_node(grid, row, column);
		ans.second = ans.first + 1;
	}
	else
	{
		edge -= grid * (grid - 1);
		int row = edge / grid;
		int column = edge - (row * grid);
		ans.first = get_node(grid, row, column);
		ans.second = ans.first + grid;	
	}
	return ans;
}

int Maze::find_rep(int n, int rep[])
{
	while(n != rep[n])
		n = rep[n];
	return n;
}

void Maze::add_edge(int x, int y, int rep[], int size[])
{
	if(size[x] >= size[y])
	{
		rep[y] = x;
		size[x] += size[y];
	}
	else
	{
		rep[x] = y;
		size[y] += size[x];
	}
}

float * Maze::getwallverts()
{
	int n = this->gridsize;
	srand(time(0));
	int rep[n * n];
	int size[n * n];
	for(int i = 0;i < n * n;i ++)
	{
		rep[i] = i;
		size[i] = 1;
	}
	int noe = 2 * n * (n - 1);
	int rantemp[noe] = {0};
	int edgelist[noe] = {0};
	for(int i = 0;i < noe;i ++)
	{
		int cur = rand() % noe;
		while(rantemp[cur] == 1)
			cur = (cur + 1) % noe;
		rantemp[cur] = 1;
		pair <int, int> cans = get_ends(n, cur);
		int tr = find_rep(cans.first, rep);
		int tc = find_rep(cans.second, rep);
		if(tr != tc)
		{
			add_edge(tr, tc, rep, size);
			edgelist[cur] = 1;
		}
	}
	int edgecount = 0;
	for(int i = 0;i < noe;i ++)
	{
		if(edgelist[i] == 0)
		{
			this->edgewalls[edgecount] = i;
			edgecount ++;
		}
	}
	this->noe = edgecount;
	this->nof = edgecount * 2 * 6;
	int curedge = 0;
	for(int i = 0;i < noe;i ++)
	{
		if(edgelist[i] == 0)
		{
			pair <int, int> ans = get_ends(n, i);
			pair <int, int> coo = get_coor(n, ans.second);
			float x1 = coo.second;
			float y1 = coo.first;
			float x2 = x1;
			float y2 = y1 + 1;
			if(ans.second - ans.first != 1)
			{
				x2 = x1 + 1;
				y2 = y1;
			}
			x1 -= (n / 2);
			y1 -= (n / 2);
			x2 -= (n / 2);
			y2 -= (n / 2);
			x1 *= (2.0 / n);
			y1 *= (2.0 / n);
			x2 *= (2.0 / n);
			y2 *= (2.0 / n);
			this->verts[12 * curedge + 0] = x1;
			this->verts[12 * curedge + 1] = -y1;
			this->verts[12 * curedge + 2] = 0.0;
			this->verts[12 * curedge + 3] = 1.0;
			this->verts[12 * curedge + 4] = 1.0;
			this->verts[12 * curedge + 5] = 1.0;
			this->verts[12 * curedge + 6] = x2;
			this->verts[12 * curedge + 7] = -y2;
			this->verts[12 * curedge + 8] = 0.0;
			this->verts[12 * curedge + 9] = 1.0;
			this->verts[12 * curedge + 10] = 1.0;
			this->verts[12 * curedge + 11] = 1.0;
			curedge ++;
		}
	}
	for(int j = 0;j < 2;j ++)
	{
		for(int i = 0;i < n;i ++)
		{
			float x1 = 0;
			if(j == 1)
				x1 = n;
			float y1 = i;
			float x2 = x1;
			float y2 = y1 + 1;
			x1 -= (n / 2);
			y1 -= (n / 2);
			x2 -= (n / 2);
			y2 -= (n / 2);
			x1 *= (2.0 / n);
			y1 *= (2.0 / n);
			x2 *= (2.0 / n);
			y2 *= (2.0 / n);
			this->verts[12 * curedge + 0] = x1;
			this->verts[12 * curedge + 1] = -y1;
			this->verts[12 * curedge + 2] = 0.0;
			this->verts[12 * curedge + 3] = 1.0;
			this->verts[12 * curedge + 4] = 1.0;
			this->verts[12 * curedge + 5] = 1.0;
			this->verts[12 * curedge + 6] = x2;
			this->verts[12 * curedge + 7] = -y2;
			this->verts[12 * curedge + 8] = 0.0;
			this->verts[12 * curedge + 9] = 1.0;
			this->verts[12 * curedge + 10] = 1.0;
			this->verts[12 * curedge + 11] = 1.0;
			curedge ++;
		}
	}
	for(int j = 0;j < 2;j ++)
	{
		for(int i = 0;i < n;i ++)
		{
			if(j == 1 && i == n - 1)
				continue;
			float x1 = i;
			float y1 = 0;
			if(j == 1)
				y1 = n;
			float x2 = x1 + 1;
			float y2 = y1;
			x1 -= (n / 2);
			y1 -= (n / 2);
			x2 -= (n / 2);
			y2 -= (n / 2);
			x1 *= (2.0 / n);
			y1 *= (2.0 / n);
			x2 *= (2.0 / n);
			y2 *= (2.0 / n);
			this->verts[12 * curedge + 0] = x1;
			this->verts[12 * curedge + 1] = -y1;
			this->verts[12 * curedge + 2] = 0.0;
			this->verts[12 * curedge + 3] = 1.0;
			this->verts[12 * curedge + 4] = 1.0;
			this->verts[12 * curedge + 5] = 1.0;
			this->verts[12 * curedge + 6] = x2;
			this->verts[12 * curedge + 7] = -y2;
			this->verts[12 * curedge + 8] = 0.0;
			this->verts[12 * curedge + 9] = 1.0;
			this->verts[12 * curedge + 10] = 1.0;
			this->verts[12 * curedge + 11] = 1.0;
			curedge ++;
		}
	}
	this->nof = curedge * 2 * 6;
	return this->verts;
}